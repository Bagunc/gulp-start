'use strict'

window.lazyLoad = () => {
  const lazyImages = [].slice.call(document.querySelectorAll("img.lazy"))

  if ("IntersectionObserver" in window) {
    let lazyImageObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
        if (entry.isIntersecting) {
          let lazyImage = entry.target
          lazyImage.src = lazyImage.dataset.src
          lazyImage.srcset = lazyImage.dataset.srcset
          lazyImage.classList.remove("lazy")
          lazyImage.classList.add("lazy--loaded")
          lazyImageObserver.unobserve(lazyImage)
        }
      })
    })

    lazyImages.forEach(function(lazyImage) {
      lazyImageObserver.observe(lazyImage);
    })
  }
}

window.onload = () => {
  window.lazyLoad()
}

const { jQuery } = window

if (jQuery) {
  jQuery($ => {
    $('#testimonials-slider').slick({
      dots: true,
      slidesToShow: 1,
      prevArrow: `<a href="#" class="slick-arrow slick-arrow--prev">
        <svg><use xlink:href="/assets/img/icons.svg#arrow-left"></use></svg>
      </a>`,
      nextArrow: `<a href="#" class="slick-arrow slick-arrow--next">
        <svg><use xlink:href="/assets/img/icons.svg#arrow-right"></use></svg>
      </a>
      `,
    })
  })
}